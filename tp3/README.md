# TP3 : Conteneurisation

## I. Docker

### 1. Install

Pour installer Docker, il faut ajouter les repos dans le fichier de sources de paquets de yum (dnf) :

```bash
[vagrant@docker ~]$  sudo yum install -y yum-utils
[...]
[vagrant@docker ~]$  sudo yum-config-manager \
>     --add-repo \
>     https://download.docker.com/linux/centos/docker-ce.repo
Adding repo from: https://download.docker.com/linux/centos/docker-ce.repo
[vagrant@docker ~]$ sudo yum install docker-ce docker-ce-cli containerd.io
[...]
```

On démarre ensuite le service avec la commande : `sudo systemctl start docker`.

On ajoute notre user au groupe docker avec la commande : `sudo usermod -aG docker $(whoami)`.

Je vérifie que j'ai bien accès à docker :

```bash
[vagrant@docker ~]$ docker info
Client:
 Context:    default
 Debug Mode: false
 Plugins:
  app: Docker App (Docker Inc., v0.9.1-beta3)
  buildx: Docker Buildx (Docker Inc., v0.8.0-docker)
  scan: Docker Scan (Docker Inc., v0.17.0)

Server:
 Containers: 0
  Running: 0
  Paused: 0
  Stopped: 0
 Images: 0
 Server Version: 20.10.13
 Storage Driver: overlay2
  Backing Filesystem: xfs
  Supports d_type: true
  Native Overlay Diff: true
  userxattr: false
 Logging Driver: json-file
 Cgroup Driver: cgroupfs
 Cgroup Version: 1
 Plugins:
  Volume: local
  Network: bridge host ipvlan macvlan null overlay
  Log: awslogs fluentd gcplogs gelf journald json-file local logentries splunk syslog
 Swarm: inactive
 Runtimes: io.containerd.runc.v2 io.containerd.runtime.v1.linux runc
 Default Runtime: runc
 Init Binary: docker-init
 containerd version: 2a1d4dbdb2a1030dc5b01e96fb110a9d9f150ecc
 runc version: v1.0.3-0-gf46b6ba
 init version: de40ad0
 Security Options:
  seccomp
   Profile: default
 Kernel Version: 4.18.0-348.20.1.el8_5.x86_64
 Operating System: Rocky Linux 8.5 (Green Obsidian)
 OSType: linux
 Architecture: x86_64
 CPUs: 2
 Total Memory: 1.774GiB
 Name: docker.tp3.cloud
 ID: SLUQ:ZSPH:FXGI:NE2C:U2FB:JYJW:MNKY:3O6K:PVRD:XUV7:ATQO:ASUH
 Docker Root Dir: /var/lib/docker
 Debug Mode: false
 Registry: https://index.docker.io/v1/
 Labels:
 Experimental: false
 Insecure Registries:
  127.0.0.0/8
 Live Restore Enabled: false
```

Comme la commande m'a répondu, j'ai donc bien les accès à Docker.

### 3. L'installation de Docker

Lorsque j'exécute la commande `docker info`, j'obtiens ce résultat :

```bash
[vagrant@docker ~]$ docker info
Client:
 Context:    default
 Debug Mode: false
 Plugins:
  app: Docker App (Docker Inc., v0.9.1-beta3)
  buildx: Docker Buildx (Docker Inc., v0.8.0-docker)
  scan: Docker Scan (Docker Inc., v0.17.0)

Server:
 Containers: 1
  Running: 0
  Paused: 0
  Stopped: 1
 Images: 1
 Server Version: 20.10.13
 Storage Driver: overlay2
  Backing Filesystem: xfs
  Supports d_type: true
  Native Overlay Diff: true
  userxattr: false
 Logging Driver: json-file
 Cgroup Driver: cgroupfs
 Cgroup Version: 1
 Plugins:
  Volume: local
  Network: bridge host ipvlan macvlan null overlay
  Log: awslogs fluentd gcplogs gelf journald json-file local logentries splunk syslog
 Swarm: inactive
 Runtimes: io.containerd.runc.v2 io.containerd.runtime.v1.linux runc
 Default Runtime: runc
 Init Binary: docker-init
 containerd version: 2a1d4dbdb2a1030dc5b01e96fb110a9d9f150ecc
 runc version: v1.0.3-0-gf46b6ba
 init version: de40ad0
 Security Options:
  seccomp
   Profile: default
 Kernel Version: 4.18.0-348.20.1.el8_5.x86_64
 Operating System: Rocky Linux 8.5 (Green Obsidian)
 OSType: linux
 Architecture: x86_64
 CPUs: 2
 Total Memory: 1.774GiB
 Name: docker.tp3.cloud
 ID: SLUQ:ZSPH:FXGI:NE2C:U2FB:JYJW:MNKY:3O6K:PVRD:XUV7:ATQO:ASUH
 Docker Root Dir: /var/lib/docker
 Debug Mode: false
 Registry: https://index.docker.io/v1/
 Labels:
 Experimental: false
 Insecure Registries:
  127.0.0.0/8
 Live Restore Enabled: false
```

Nous pouvons constater que la ligne `Docker Root Dir: /var/lib/docker` nous indique que l'emplacement des fichiers est `/var/lib/docker`.

Docker exécute les commandes à l'aide d'un socket. Nous pouvons vérifier les droits sur ce fichier :

```bash
[vagrant@docker ~]$ sudo ls -al /var/run/docker.sock
srw-rw----. 1 root docker 0 Mar 14 10:45 /var/run/docker.sock
```

Nous pouvons constater que ce fichier appartient à `root` mais également au groupe `docker`. DOnc pour pouvoir taper des commandes, donc utiliser le socket, il faut faire partie du groupe `docker`.

Le fichier de configuration de docker se situe ici : `/etc/docker/daemon.json`.

J'ai modifié le fichier de configuration de docker :

```json
{
        "oom-score-adjust": -400,
        "graph": "/home/vagrant/docker"
}
```

Je redémarre le service avec la commande : `sudo systemctl restart docker` et je regarde ce qu'il y a dans mon dossier docker :

```bash
[vagrant@docker ~]$ sudo ls docker/
buildkit  containers  image  network  overlay2  plugins  runtimes  swarm  tmp  trust  volumes
[vagrant@docker ~]$ sudo cat /etc/docker/daemon.json
{
        "oom-score-adjust": -400,
        "graph": "/home/vagrant/docker"
}
```

Je regarde les processus de Docker :

```bash
[vagrant@docker ~]$ ps -ef | grep docker
root        1865       1  0 11:43 ?        00:00:00 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
vagrant     2011    1335  0 11:47 pts/0    00:00:00 grep --color=auto docker
```

JE regarde les processus de mon container :
container lancé avec la commande : docker run -d debian sleep 99999

```bash
[vagrant@docker ~]$ ps -ef | grep container
root        1163       1  0 10:45 ?        00:00:03 /usr/bin/containerd
root        1865       1  0 11:43 ?        00:00:01 /usr/bin/dockerd -H fd:// --containerd=/run/containerd/containerd.sock
root        2057       1  0 11:48 ?        00:00:00 /usr/bin/containerd-shim-runc-v2 -namespace moby -id c949c2fd3f7c4b1914c8f7d25113af9a98c02c24f23ba910b039ea1d897b45f6 -address /run/containerd/containerd.sock
vagrant     2109    1335  0 11:49 pts/0    00:00:00 grep --color=auto container
[vagrant@docker ~]$ ps -ef | grep sleep
root        2076    2057  0 11:48 ?        00:00:00 sleep 99999
vagrant     2111    1335  0 11:50 pts/0    00:00:00 grep --color=auto sleep
```

### 4. Lancement de conteneurs

La commande à taper est `docker run --name web -d -v /home/vagrant/files/html/:/usr/share/nginx/html/ -v /home/vagrant/files/conf/:/etc/nginx/ -p 8080:8080 --memory="500m" --cpu-shares="500" -u wwwdata nginx`.

## II. Images

J'ai utilisé l'image Alpine qui est très légère car elle ne contient que le strict minimum.
Pour créer mon image j'ai utilisé cette commande : `docker build -t alpine_test .`

<a href="files/Dockerfile">DOckerfile</a>

Une fois mon image créée, je teste de lancer mon container avec la commande : `docker container run -p 8080:80 -d alpine_test`.

## III. `docker-compose`

Pour mettre en place le service, j'ai réutilisé l'image alpine créée précédemment, j'y ai installé l'application nextcloud et j'ai utilisé le service `MariaDB`.
<a href="files/docker-compoose.yml">Docker compose</a>
