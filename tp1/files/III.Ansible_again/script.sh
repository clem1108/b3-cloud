#!/bin/bash

# Install Docker
sudo apt-get update -y

sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release -y

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update -y

sudo apt-get install docker-ce docker-ce-cli containerd.io -y

sudo systemctl enable --now docker

# Create User

useradd --system --shell /bin/bash --home /home/docker-admin --group docker docker-admin
echo "docker-admin     ALL=(ALL)  NOPASSWD: ALL" >> /etc/sudoers

usermod -aG docker docker-admin

mkdir -p /home/docker-admin/.ssh

echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCv/mdnBeRf5z6iw3y2umUh/PXwQhteAtBuKnK5vTwg/tpd+t99j2nLfFIEARvJGHvhzm9cOJjVzAurvK43H1hXdQSNywmGPKPQnDgSOkouh88f0CLRBby1dH+Zwpnjq1nSHLCP91cjymmzWYs++i/1vKd7IVa3GfFsXC9Mz4mOG6RrDOaa1Xf81Pb6tOYgbuCd2i/S7MwZHfqU4jQN/oAjxkMobd2DExxhHNK606jEj26NgBvVpCKm6OkAuXrOXmCKCXIObeLPwP1UrNcSafCFDCVX7xRa541b5Y5og1BSmjHzDBB4q1SRJVOlCmXpb/ooNkr5LX9jZIFoXZf1InPTNMdOjDA9HiXqLYVHrQ4xldBacee+DdWKOfBqnTpDrGYiSUMB1bg+DSsBvdF5vRNKaqNgFFeAl+Qi15pus4t6OAQxJM3q7QnRBnhL2UyP8HFcG+zCh47h9sJKk11KT8sv8IhBZUHyA4exQzkgS5XKRdCEnqTTQYVRmMoKDysUTrM= cleme@MSI" >> /home/docker-admin/.ssh/authorized_keys

# Ansible

sudo apt install -y python

useradd --system --shell /bin/bash --home /home/ansible-admin --group admin ansible-admin
echo "ansible-admin     ALL=(ALL)  NOPASSWD: ALL" >> /etc/sudoers

mkdir -p /home/ansible-admin/.ssh

echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCv/mdnBeRf5z6iw3y2umUh/PXwQhteAtBuKnK5vTwg/tpd+t99j2nLfFIEARvJGHvhzm9cOJjVzAurvK43H1hXdQSNywmGPKPQnDgSOkouh88f0CLRBby1dH+Zwpnjq1nSHLCP91cjymmzWYs++i/1vKd7IVa3GfFsXC9Mz4mOG6RrDOaa1Xf81Pb6tOYgbuCd2i/S7MwZHfqU4jQN/oAjxkMobd2DExxhHNK606jEj26NgBvVpCKm6OkAuXrOXmCKCXIObeLPwP1UrNcSafCFDCVX7xRa541b5Y5og1BSmjHzDBB4q1SRJVOlCmXpb/ooNkr5LX9jZIFoXZf1InPTNMdOjDA9HiXqLYVHrQ4xldBacee+DdWKOfBqnTpDrGYiSUMB1bg+DSsBvdF5vRNKaqNgFFeAl+Qi15pus4t6OAQxJM3q7QnRBnhL2UyP8HFcG+zCh47h9sJKk11KT8sv8IhBZUHyA4exQzkgS5XKRdCEnqTTQYVRmMoKDysUTrM= cleme@MSI" >> /home/ansible-admin/.ssh/authorized_keys

# NTP

 sudo apt install ntp -y

echo "logfile /var/log/ntpstats/ntpd
statsdir /var/log/ntpstats/
statistics loopstats peerstats clockstats
filegen loopstats file loopstats type day enable
filegen peerstats file peerstats type day enable
filegen clockstats file clockstats type day enable

# Addresses to listen on (ntpd does not listen by default)
listen on *
#listen on 127.0.0.1
#listen on ::1

# sync to a single server
#server ntp.example.org

# use a random selection of 4 public stratum 2 servers
# see http://twiki.ntp.org/bin/view/Servers/NTPPoolServers
# and http://www.pool.ntp.org/
server 0.pool.ntp.org
server 1.pool.ntp.org
server 2.pool.ntp.org
server 3.pool.ntp.org
" > sudo tee /etc/ntp.conf

sudo systemctl restart ntp

sudo ntpq -p