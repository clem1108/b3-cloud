#!/bin/bash

# Install Docker
sudo apt-get update -y

sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release -y

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update -y

sudo apt-get install docker-ce docker-ce-cli containerd.io -y

sudo systemctl enable --now docker

# Create User

useradd --system --shell /bin/bash --home /home/docker-admin --group docker docker-admin
echo "docker-admin     ALL=(ALL)  NOPASSWD: ALL" >> /etc/sudoers

usermod -aG docker docker-admin

mkdir -p /home/docker-admin/.ssh

echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCv/mdnBeRf5z6iw3y2umUh/PXwQhteAtBuKnK5vTwg/tpd+t99j2nLfFIEARvJGHvhzm9cOJjVzAurvK43H1hXdQSNywmGPKPQnDgSOkouh88f0CLRBby1dH+Zwpnjq1nSHLCP91cjymmzWYs++i/1vKd7IVa3GfFsXC9Mz4mOG6RrDOaa1Xf81Pb6tOYgbuCd2i/S7MwZHfqU4jQN/oAjxkMobd2DExxhHNK606jEj26NgBvVpCKm6OkAuXrOXmCKCXIObeLPwP1UrNcSafCFDCVX7xRa541b5Y5og1BSmjHzDBB4q1SRJVOlCmXpb/ooNkr5LX9jZIFoXZf1InPTNMdOjDA9HiXqLYVHrQ4xldBacee+DdWKOfBqnTpDrGYiSUMB1bg+DSsBvdF5vRNKaqNgFFeAl+Qi15pus4t6OAQxJM3q7QnRBnhL2UyP8HFcG+zCh47h9sJKk11KT8sv8IhBZUHyA4exQzkgS5XKRdCEnqTTQYVRmMoKDysUTrM= cleme@MSI" >> /home/docker-admin/.ssh/authorized_keys

# Ansible

sudo apt install -y python

useradd --system --shell /bin/bash --home /home/ansible-admin --group admin ansible-admin
echo "ansible-admin     ALL=(ALL)  NOPASSWD: ALL" >> /etc/sudoers

mkdir -p /home/ansible-admin/.ssh

echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCv/mdnBeRf5z6iw3y2umUh/PXwQhteAtBuKnK5vTwg/tpd+t99j2nLfFIEARvJGHvhzm9cOJjVzAurvK43H1hXdQSNywmGPKPQnDgSOkouh88f0CLRBby1dH+Zwpnjq1nSHLCP91cjymmzWYs++i/1vKd7IVa3GfFsXC9Mz4mOG6RrDOaa1Xf81Pb6tOYgbuCd2i/S7MwZHfqU4jQN/oAjxkMobd2DExxhHNK606jEj26NgBvVpCKm6OkAuXrOXmCKCXIObeLPwP1UrNcSafCFDCVX7xRa541b5Y5og1BSmjHzDBB4q1SRJVOlCmXpb/ooNkr5LX9jZIFoXZf1InPTNMdOjDA9HiXqLYVHrQ4xldBacee+DdWKOfBqnTpDrGYiSUMB1bg+DSsBvdF5vRNKaqNgFFeAl+Qi15pus4t6OAQxJM3q7QnRBnhL2UyP8HFcG+zCh47h9sJKk11KT8sv8IhBZUHyA4exQzkgS5XKRdCEnqTTQYVRmMoKDysUTrM= cleme@MSI" >> /home/ansible-admin/.ssh/authorized_keys

# NTP

 sudo apt install chrony -y

echo "# Welcome to the chrony configuration file. See chrony.conf(5) for more
# information about usuable directives.

# This will use (up to):
# - 4 sources from ntp.ubuntu.com which some are ipv6 enabled
# - 2 sources from 2.ubuntu.pool.ntp.org which is ipv6 enabled as well
# - 1 source from [01].ubuntu.pool.ntp.org each (ipv4 only atm)
# This means by default, up to 6 dual-stack and up to 2 additional IPv4-only
# sources will be used.
# At the same time it retains some protection against one of the entries being
# down (compare to just using one of the lines). See (LP: #1754358) for the
# discussion.
#
# About using servers from the NTP Pool Project in general see (LP: #104525).
# Approved by Ubuntu Technical Board on 2011-02-08.
# See http://www.pool.ntp.org/join.html for more information.
# Use servers from the NTP Pool Project. Approved by Ubuntu Technical Board
# on 2011-02-08 (LP: #104525). See http://www.pool.ntp.org/join.html for
# more information.
server 0.pool.ntp.org
server 1.pool.ntp.org
server 2.pool.ntp.org
server 3.pool.ntp.org

# This directive specify the location of the file containing ID/key pairs for
# NTP authentication.
keyfile /etc/chrony/chrony.keys

# This directive specify the file into which chronyd will store the rate
# information.
driftfile /var/lib/chrony/chrony.drift

# Uncomment the following line to turn logging on.
#log tracking measurements statistics

# Log files location.
logdir /var/log/chrony

# Stop bad estimates upsetting machine clock.
maxupdateskew 100.0

# This directive enables kernel synchronisation (every 11 minutes) of the
# real-time clock. Note that it can’t be used along with the 'rtcfile' directive.
rtcsync

# Step the system clock instead of slewing it if the adjustment is larger than
# one second, but only in the first three clock updates.
makestep 1 3" | tee /etc/chrony/chrony.conf

sudo timedatectl set-ntp true
sudo systemctl restart chrony.service

sudo chronyd -q