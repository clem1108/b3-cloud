# TP1 : cloud-init

## I. Premiers pas

Une fois les fichiers créés et l'exécution effectuée, procédons à la vérification :

```bash
vagrant@ubuntu:~$ cat /tmp/b3_git_version
git version 2.25.1
vagrant@ubuntu:~$ cat /etc/passwd | grep it4
it4:x:1001:1001::/home/it4:/bin/bash
vagrant@ubuntu:~$ whereis git
git: /usr/bin/git /usr/share/man/man1/git.1.gz
vagrant@ubuntu:~$
```

## II. Go further

Le nouveau Vagrantfile est :

```ruby
Vagrant.configure("2") do |config|
    config.env.enable
  
    config.vm.define "ubuntu-cloud-init"
    config.vm.box = "focal-server-cloudimg-amd64-vagrant"
    config.vm.box_url = "https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64-vagrant.box"
    # Ajoutez cette ligne afin d'accélérer le démarrage de la VM (si une erreur 'vbguest' est levée, voir la note un peu plus bas)
    config.vbguest.auto_update = false
    # Désactive les updates auto qui peuvent ralentir le lancement de la machine
    config.vm.box_check_update = false 
    config.vm.provider "virtualbox" do |v|
      v.name = "ubuntu-cloud-init"
    end

    config.vm.cloud_init do |cloud_init|
      cloud_init.content_type = "text/cloud-config"
      cloud_init.path = "data.yml"
      config.vm.network "private_network", ip: "192.168.5.10"
      config.vm.provision "shell", path: "script.sh"
    end
  end
```

<a href="files/II.Go_further/Vagrantfile">VagrantFile</a>

on y définit une adresse IP, le fichier que cloud_init va utiliser pour provisionner, provisionner la machine avec un script.

Le script utilisé est :

```bash
#!/bin/bash

# Install Docker
sudo apt-get update -y

sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release -y

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg

echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null

sudo apt-get update -y

sudo apt-get install docker-ce docker-ce-cli containerd.io -y

sudo systemctl enable --now docker

# Create User

useradd --system --shell /bin/bash --home /home/docker-admin --group docker docker-admin
echo "docker-admin     ALL=(ALL)  NOPASSWD: ALL" >> /etc/sudoers

usermod -aG docker docker-admin

mkdir -p /home/docker-admin/.ssh

echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCv/mdnBeRf5z6iw3y2umUh/PXwQhteAtBuKnK5vTwg/tpd+t99j2nLfFIEARvJGHvhzm9cOJjVzAurvK43H1hXdQSNywmGPKPQnDgSOkouh88f0CLRBby1dH+Zwpnjq1nSHLCP91cjymmzWYs++i/1vKd7IVa3GfFsXC9Mz4mOG6RrDOaa1Xf81Pb6tOYgbuCd2i/S7MwZHfqU4jQN/oAjxkMobd2DExxhHNK606jEj26NgBvVpCKm6OkAuXrOXmCKCXIObeLPwP1UrNcSafCFDCVX7xRa541b5Y5og1BSmjHzDBB4q1SRJVOlCmXpb/ooNkr5LX9jZIFoXZf1InPTNMdOjDA9HiXqLYVHrQ4xldBacee+DdWKOfBqnTpDrGYiSUMB1bg+DSsBvdF5vRNKaqNgFFeAl+Qi15pus4t6OAQxJM3q7QnRBnhL2UyP8HFcG+zCh47h9sJKk11KT8sv8IhBZUHyA4exQzkgS5XKRdCEnqTTQYVRmMoKDysUTrM= cleme@MSI" >> /home/docker-admin/.ssh/authorized_keys

# Ansible

sudo apt install -y python

useradd --system --shell /bin/bash --home /home/ansible-admin --group admin ansible-admin
echo "ansible-admin     ALL=(ALL)  NOPASSWD: ALL" >> /etc/sudoers

mkdir -p /home/ansible-admin/.ssh

echo "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQCv/mdnBeRf5z6iw3y2umUh/PXwQhteAtBuKnK5vTwg/tpd+t99j2nLfFIEARvJGHvhzm9cOJjVzAurvK43H1hXdQSNywmGPKPQnDgSOkouh88f0CLRBby1dH+Zwpnjq1nSHLCP91cjymmzWYs++i/1vKd7IVa3GfFsXC9Mz4mOG6RrDOaa1Xf81Pb6tOYgbuCd2i/S7MwZHfqU4jQN/oAjxkMobd2DExxhHNK606jEj26NgBvVpCKm6OkAuXrOXmCKCXIObeLPwP1UrNcSafCFDCVX7xRa541b5Y5og1BSmjHzDBB4q1SRJVOlCmXpb/ooNkr5LX9jZIFoXZf1InPTNMdOjDA9HiXqLYVHrQ4xldBacee+DdWKOfBqnTpDrGYiSUMB1bg+DSsBvdF5vRNKaqNgFFeAl+Qi15pus4t6OAQxJM3q7QnRBnhL2UyP8HFcG+zCh47h9sJKk11KT8sv8IhBZUHyA4exQzkgS5XKRdCEnqTTQYVRmMoKDysUTrM= cleme@MSI" >> /home/ansible-admin/.ssh/authorized_keys

# NTP

 sudo apt install chrony -y

echo "# Welcome to the chrony configuration file. See chrony.conf(5) for more
# information about usuable directives.

# This will use (up to):
# - 4 sources from ntp.ubuntu.com which some are ipv6 enabled
# - 2 sources from 2.ubuntu.pool.ntp.org which is ipv6 enabled as well
# - 1 source from [01].ubuntu.pool.ntp.org each (ipv4 only atm)
# This means by default, up to 6 dual-stack and up to 2 additional IPv4-only
# sources will be used.
# At the same time it retains some protection against one of the entries being
# down (compare to just using one of the lines). See (LP: #1754358) for the
# discussion.
#
# About using servers from the NTP Pool Project in general see (LP: #104525).
# Approved by Ubuntu Technical Board on 2011-02-08.
# See http://www.pool.ntp.org/join.html for more information.
# Use servers from the NTP Pool Project. Approved by Ubuntu Technical Board
# on 2011-02-08 (LP: #104525). See http://www.pool.ntp.org/join.html for
# more information.
server 0.pool.ntp.org
server 1.pool.ntp.org
server 2.pool.ntp.org
server 3.pool.ntp.org

# This directive specify the location of the file containing ID/key pairs for
# NTP authentication.
keyfile /etc/chrony/chrony.keys

# This directive specify the file into which chronyd will store the rate
# information.
driftfile /var/lib/chrony/chrony.drift

# Uncomment the following line to turn logging on.
#log tracking measurements statistics

# Log files location.
logdir /var/log/chrony

# Stop bad estimates upsetting machine clock.
maxupdateskew 100.0

# This directive enables kernel synchronisation (every 11 minutes) of the
# real-time clock. Note that it can’t be used along with the 'rtcfile' directive.
rtcsync

# Step the system clock instead of slewing it if the adjustment is larger than
# one second, but only in the first three clock updates.
makestep 1 3" | tee /etc/chrony/chrony.conf

sudo timedatectl set-ntp true
sudo systemctl restart chrony.service

sudo chronyd -q
```

<a href="files/II.Go_further/script.sh">VagrantFile</a>

## III. Ansible again

Le nouveau Vagrantfile est :

```ruby
Vagrant.configure("2") do |config|
  config.env.enable

  config.vm.define "ubuntu-cloud-init" do |cloud|
    cloud.vm.box = "focal-server-cloudimg-amd64-vagrant"
    cloud.vm.box_url = "https://cloud-images.ubuntu.com/focal/current/focal-server-cloudimg-amd64-vagrant.box"
  # Ajoutez cette ligne afin d'accélérer le démarrage de la VM (si une erreur 'vbguest' est levée, voir la note un peu plus bas)
    cloud.vbguest.auto_update = false
  # Désactive les updates auto qui peuvent ralentir le lancement de la machine
    cloud.vm.box_check_update = false 
    cloud.vm.provider "virtualbox" do |v|
      v.name = "ubuntu-cloud-init"
    end

    cloud.vm.cloud_init do |cloud_init|
      cloud_init.content_type = "text/cloud-config"
      cloud_init.path = "data.yml"
      cloud_init.vm.network "private_network", ip: "192.168.5.10"
      config.vm.provision "shell", path: "script.sh"
      cloud_init.vm.provision "shell", path: "script_ansible.sh"
    end
  end

  config.vm.define "node1" do |ansible|
    ansible.vm.box = "centos-ynov"
    ansible.vbguest.auto_update = false
    ansible.vm.box_check_update = false 
    ansible.vm.synced_folder ".", "/vagrant", disabled: true
    ansible.vm.hostname = "node1.tp1.ynov"
    ansible.vm.network "private_network", ip: "192.168.5.11"
    ansible.vm.provider "virtualbox" do |v2|
      v2.name = "node1"
    end
  end
end
```

<a href="files/III.Ansible_again/Vagrantfile">VagrantFile</a>

On y définit deux machines avec des IP, hostname et script de démarrage différents. Le nouveau script utilisé sur la machine cloud est :

```bash
# install ansible
sudo apt install ansible -y

# Create ansible files

mkdir -p /home/ansible-admin/ansible

# create hosts.ini

echo "[ynov]
192.168.5.11" > /home/ansible-admin/ansible/hosts.ini

# create html file

echo "Hello from {{ ansible_default_ipv4.address }}
" > /home/ansible-admin/ansible/index.html.j2

# create deployment file

echo "---
- name: Install nginx
  hosts: ynov
  become: true

  tasks:
  - name: Install nginx
    apt:
      name: nginx
      state: present

  - name: ansible create directory example
    file:
      path: /var/www/b3_tp1/
      state: directory

  - name: Insert Index Page
    template:
      src: index.html.j2
      dest: /var/www/b3_tp1/index.html

  - name: Insert config
    template:
      src: nginx.conf
      dest: /etc/nginx/sites-enabled/nginx.conf

  - name: Start NGiNX
    service:
      name: nginx
      state: restarted
" > /home/ansible-admin/ansible/nginx.yml

# create nginx config file

echo "# For more information on configuration, see:
#   * Official English Documentation: http://nginx.org/en/docs/
#   * Official Russian Documentation: http://nginx.org/ru/docs/

user nginx;
worker_processes auto;
error_log /var/log/nginx/error.log;
pid /run/nginx.pid;

# Load dynamic modules. See /usr/share/doc/nginx/README.dynamic.
include /usr/share/nginx/modules/*.conf;

events {
    worker_connections 1024;
}

http {
    log_format  main  '$remote_addr - $remote_user [$time_local] "$request" '
                      '$status $body_bytes_sent "$http_referer" '
                      '"$http_user_agent" "$http_x_forwarded_for"';

    access_log  /var/log/nginx/access.log  main;

    sendfile            on;
    tcp_nopush          on;
    tcp_nodelay         on;
    keepalive_timeout   65;
    types_hash_max_size 4096;

    include             /etc/nginx/mime.types;
    default_type        application/octet-stream;

    # Load modular configuration files from the /etc/nginx/conf.d directory.
    # See http://nginx.org/en/docs/ngx_core_module.html#include
    # for more information.
    include /etc/nginx/conf.d/*.conf;

    server {
        listen       8080;
        listen       [::]:8080;
        server_name  _;
        root         /var/www/b3_tp1/;

        # Load configuration files for the default server block.
        include /etc/nginx/default.d/*.conf;

        error_page 404 /404.html;
        location = /404.html {
        }

        error_page 500 502 503 504 /50x.html;
        location = /50x.html {
        }
    }

# Settings for a TLS enabled server.
#
#    server {
#        listen       443 ssl http2;
#        listen       [::]:443 ssl http2;
#        server_name  _;
#        root         /usr/share/nginx/html;
#
#        ssl_certificate "/etc/pki/nginx/server.crt";
#        ssl_certificate_key "/etc/pki/nginx/private/server.key";
#        ssl_session_cache shared:SSL:1m;
#        ssl_session_timeout  10m;
#        ssl_ciphers HIGH:!aNULL:!MD5;
#        ssl_prefer_server_ciphers on;
#
#        # Load configuration files for the default server block.
#        include /etc/nginx/default.d/*.conf;
#
#        error_page 404 /404.html;
#            location = /40x.html {
#        }
#
#        error_page 500 502 503 504 /50x.html;
#            location = /50x.html {
#        }
#    }

}
" > /home/ansible-admin/ansible/nginx.conf
```

<a href="files/III.Ansible_again/script_ansible.sh">Script</a>

J'ai déposé la clé SSH du cloud-init vers le node avec la commande `ssh-copy-id -i /home/vagrant/.ssh/id_rsa.pub vagrant@192.168.5.11`.

Je me suis ensuite positionné dans le dossier où sont placés tous les fichiers et j'ai lancé l'exécution avec la commande `ansible-playbook -i hosts.ini nginx.yml`.
