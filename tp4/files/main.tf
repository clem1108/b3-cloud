terraform {
  required_providers {
    azurerm = {
      source  = "hashicorp/azurerm"
      version = ">=3.0.0"
    }
  }
}

provider "azurerm" {
  features {}
}

resource "azurerm_resource_group" "rg-b3-vm" {
  name     = "b3-vm"
  location = "eastus"
}

resource "azurerm_virtual_network" "vn-b3-vm" {
  name                = "b3-vm"
  address_space       = ["10.0.0.0/16"]
  location            = azurerm_resource_group.rg-b3-vm.location
  resource_group_name = azurerm_resource_group.rg-b3-vm.name
}

resource "azurerm_subnet" "s-b3-vm" {
  name                 = "internal"
  resource_group_name  = azurerm_resource_group.rg-b3-vm.name
  virtual_network_name = azurerm_virtual_network.vn-b3-vm.name
  address_prefixes     = ["10.0.2.0/24"]
}

resource "azurerm_public_ip" "public_ip" {
  name                    = "publicipvm1"
  resource_group_name     = azurerm_resource_group.rg-b3-vm.name
  location                = azurerm_resource_group.rg-b3-vm.location
  allocation_method       = "Dynamic"
  idle_timeout_in_minutes = 30
}
