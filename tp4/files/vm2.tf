resource "azurerm_network_interface" "nic-b3-vm2" {
  name                = "nic-vm2"
  location            = azurerm_resource_group.rg-b3-vm.location
  resource_group_name = azurerm_resource_group.rg-b3-vm.name

  ip_configuration {
    name                          = "internal"
    subnet_id                     = azurerm_subnet.s-b3-vm.id
    private_ip_address_allocation = "Dynamic"
  }
}

resource "azurerm_linux_virtual_machine" "vm-b3-vm2" {
  name                = "b3-vm2"
  resource_group_name = azurerm_resource_group.rg-b3-vm.name
  location            = azurerm_resource_group.rg-b3-vm.location
  size                = "Standard_B1s"
  admin_username      = "clement"
  network_interface_ids = [
    azurerm_network_interface.nic-b3-vm2.id,
  ]

  admin_ssh_key {
    username   = "clement"
    public_key = file("C:/Users/cleme/.ssh/id_rsa.pub")
  }

  os_disk {
    caching              = "ReadWrite"
    storage_account_type = "Standard_LRS"
  }

  source_image_reference {
    publisher = "OpenLogic"
    offer     = "CentOS"
    sku       = "7.5"
    version   = "latest"
  }
}
