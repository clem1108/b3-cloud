# Terraform

Pour mettre en place l'infrastructure j'ai divisé en 3 fichiers :

* Un fichier commun pour les paramètres communs <a href="files/main.tf">main.tf</a>
* Un fichier dédié à la VM 1 (OS : Ubuntu + IP public en plus) <a href="files/vm1.tf">vm1.tf</a>
* Un fichier dédié à la VM 2 <a href="files/vm2.tf">vm2.tf</a>
